﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace IEduca.Test.Transversal.Common
{
    public interface IConnectionFactory
    {
        IDbConnection GetConnection { get; }
        IDbConnection GetMysqlConnection { get; }
    }
}
