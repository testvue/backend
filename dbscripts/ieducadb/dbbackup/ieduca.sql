-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 10-06-2021 a las 17:34:00
-- Versión del servidor: 5.7.31
-- Versión de PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ieduca`
--

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `SP_ClientDelete`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ClientDelete` (IN `p_clientid` INT)  BEGIN 
 UPDATE client 
	SET state = 0
  WHERE clientid = p_clientid; 

END$$

DROP PROCEDURE IF EXISTS `SP_ClientFamiliyMemberDelete`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ClientFamiliyMemberDelete` (IN `p_clientfamilymemberid` INT)  BEGIN 
 UPDATE ClientFamiliyMemberDelete 
	SET state = 0
  WHERE clientfamilymemberid = p_clientfamilymemberid; 

END$$

DROP PROCEDURE IF EXISTS `SP_ClientFamilyMemberGetByID`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ClientFamilyMemberGetByID` (IN `p_clientid` INT)  BEGIN
	SELECT *  FROM clientfamilymember WHERE  clientfamilymemberid = p_clientfamilymemberid;
END$$

DROP PROCEDURE IF EXISTS `SP_ClientFamilyMemberInsert`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ClientFamilyMemberInsert` (IN `p_name` VARCHAR(191), IN `p_lastname` VARCHAR(191), IN `p_membertype` INT, IN `p_clientid` INT)  BEGIN
insert into client(name, lastname,membertype, clientid) values (p_name, p_lastname,p_membertype, p_clientid);
END$$

DROP PROCEDURE IF EXISTS `SP_ClientFamilyMemberList`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ClientFamilyMemberList` (IN `p_clientid` INT)  BEGIN
	SELECT *  FROM clientfamilymember WHERE  clientid = p_clientid;
END$$

DROP PROCEDURE IF EXISTS `SP_ClientFamilyMemberUpdate`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ClientFamilyMemberUpdate` (IN `p_clientfamilymemberid` INT, IN `p_clientid` INT, IN `p_name` VARCHAR(191), IN `p_lastname` VARCHAR(191), IN `p_membertype` INT)  NO SQL
UPDATE ClientFamilyMember 
	SET name = p_name,
        lastname = p_lastname,
        clientid = p_clientid,
        membertype = p_membertype
  WHERE clientfamilymemberid = p_clientfamilymemberid$$

DROP PROCEDURE IF EXISTS `SP_ClientGetByID`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ClientGetByID` (IN `p_clientid` INT)  NO SQL
BEGIN
	SELECT *  FROM client WHERE state = 1 and clientid = p_clientid;
END$$

DROP PROCEDURE IF EXISTS `SP_ClientInsert`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ClientInsert` (IN `p_name` VARCHAR(191), IN `p_lastname` VARCHAR(191), IN `p_Address` VARCHAR(250), IN `p_phone` VARCHAR(20), IN `p_email` VARCHAR(191))  BEGIN
insert into client(name, lastname, address, phone,email) values (p_name, p_lastname,p_address, p_phone,p_email);
END$$

DROP PROCEDURE IF EXISTS `SP_ClientList`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ClientList` ()  BEGIN
	SELECT *  FROM client WHERE state =1;
END$$

DROP PROCEDURE IF EXISTS `SP_ClientUpdate`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ClientUpdate` (IN `p_clientid` INT, IN `p_name` VARCHAR(191), IN `p_lastname` VARCHAR(191), IN `p_address` VARCHAR(250), IN `p_phone` INT(20), IN `p_email` VARCHAR(191))  NO SQL
BEGIN 
 UPDATE client 
	SET name = p_name,
        lastname = p_lastname,
        address = p_address,
        phone = p_phone,
        email = p_email
  WHERE clientid = p_clientid; 

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `clientid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `lastname` varchar(191) NOT NULL,
  `phone` varchar(191) NOT NULL,
  `address` varchar(250) NOT NULL,
  `email` varchar(191) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '0:inactive, 1:active',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`clientid`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `client`
--

INSERT INTO `client` (`clientid`, `name`, `lastname`, `phone`, `address`, `email`, `state`, `create_date`, `update_date`) VALUES
(10, 'Carlos', 'Obregon', '5555', 'calle san patricio', 'caobregon@gmail.com', 1, '2021-06-10 07:12:25', NULL),
(11, 'Alberto', 'Obregon', '5555', 'calle san patricio', 'caobregon@gmail.com', 1, '2021-06-10 10:21:11', NULL),
(12, 'Leticia', 'Castellanos', '5555', 'calle san patricio', 'caobregon@gmail.com', 1, '2021-06-10 10:22:23', NULL),
(13, 'Leticia', 'Castellanos', '5555', 'calle san patricio', 'caobregon@gmail.com', 1, '2021-06-10 10:25:41', NULL),
(14, 'Leticiasss', 'Castellanos', '5555', 'calle san patricio', 'caobregon@gmail.com', 0, '2021-06-10 11:16:15', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientfamilymember`
--

DROP TABLE IF EXISTS `clientfamilymember`;
CREATE TABLE IF NOT EXISTS `clientfamilymember` (
  `clientfamilymemberid` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) NOT NULL,
  `membertype` int(11) NOT NULL,
  `name` varchar(191) NOT NULL,
  `lastname` varchar(191) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`clientfamilymemberid`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientfamilymember`
--

INSERT INTO `clientfamilymember` (`clientfamilymemberid`, `clientid`, `membertype`, `name`, `lastname`, `create_date`, `update_date`) VALUES
(10, 10, 2, 'Maddox', 'Obregon', '2021-06-10 07:13:55', NULL),
(11, 10, 2, 'Rocco', 'Obregon', '2021-06-10 07:14:11', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
