DELIMITER $$

DROP PROCEDURE IF EXISTS `SP_ClientFamiliyMemberDelete`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ClientFamiliyMemberDelete` (IN `p_clientfamilymemberid` INT)  BEGIN 
 UPDATE ClientFamiliyMemberDelete 
	SET state = 0
  WHERE clientfamilymemberid = p_clientfamilymemberid; 

END$$