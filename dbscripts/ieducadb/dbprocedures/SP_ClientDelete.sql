DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `SP_ClientDelete`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ClientDelete` (IN `p_clientid` INT)  BEGIN 
 UPDATE client 
	SET state = 0
  WHERE clientid = p_clientid; 

END$$