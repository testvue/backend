DELIMITER $$

DROP PROCEDURE IF EXISTS `SP_ClientFamilyMemberList`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ClientFamilyMemberList` (IN `p_clientid` INT)  BEGIN
	SELECT *  FROM clientfamilymember WHERE  clientid = p_clientid;
END$$