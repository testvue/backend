DELIMITER $$

DROP PROCEDURE IF EXISTS `SP_ClientInsert`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ClientInsert` (IN `p_name` VARCHAR(191), IN `p_lastname` VARCHAR(191), IN `p_Address` VARCHAR(250), IN `p_phone` VARCHAR(20), IN `p_email` VARCHAR(191))  BEGIN
insert into client(name, lastname, address, phone,email) values (p_name, p_lastname,p_address, p_phone,p_email);
END$$