DELIMITER $$

DROP PROCEDURE IF EXISTS `SP_ClientFamilyMemberUpdate`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ClientFamilyMemberUpdate` (IN `p_clientfamilymemberid` INT, IN `p_clientid` INT, IN `p_name` VARCHAR(191), IN `p_lastname` VARCHAR(191), IN `p_membertype` INT)  NO SQL
UPDATE ClientFamilyMember 
	SET name = p_name,
        lastname = p_lastname,
        clientid = p_clientid,
        membertype = p_membertype
  WHERE clientfamilymemberid = p_clientfamilymemberid$$