DELIMITER $$

DROP PROCEDURE IF EXISTS `SP_ClientFamilyMemberGetByID`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ClientFamilyMemberGetByID` (IN `p_clientid` INT)  BEGIN
	SELECT *  FROM clientfamilymember WHERE  clientfamilymemberid = p_clientfamilymemberid;
END$$