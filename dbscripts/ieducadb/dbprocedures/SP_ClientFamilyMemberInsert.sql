DELIMITER $$

DROP PROCEDURE IF EXISTS `SP_ClientFamilyMemberInsert`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ClientFamilyMemberInsert` (IN `p_name` VARCHAR(191), IN `p_lastname` VARCHAR(191), IN `p_membertype` INT, IN `p_clientid` INT)  BEGIN
insert into client(name, lastname,membertype, clientid) values (p_name, p_lastname,p_membertype, p_clientid);
END$$