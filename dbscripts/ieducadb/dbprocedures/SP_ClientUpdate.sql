DELIMITER $$

DROP PROCEDURE IF EXISTS `SP_ClientUpdate`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ClientUpdate` (IN `p_clientid` INT, IN `p_name` VARCHAR(191), IN `p_lastname` VARCHAR(191), IN `p_address` VARCHAR(250), IN `p_phone` INT(20), IN `p_email` VARCHAR(191))  NO SQL
BEGIN 
 UPDATE client 
	SET name = p_name,
        lastname = p_lastname,
        address = p_address,
        phone = p_phone,
        email = p_email
  WHERE clientid = p_clientid; 

END$$