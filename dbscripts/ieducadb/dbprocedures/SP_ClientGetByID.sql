DELIMITER $$

DROP PROCEDURE IF EXISTS `SP_ClientGetByID`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ClientGetByID` (IN `p_clientid` INT)  NO SQL
BEGIN
	SELECT *  FROM client WHERE state = 1 and clientid = p_clientid;
END$$